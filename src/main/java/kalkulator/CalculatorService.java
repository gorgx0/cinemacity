package kalkulator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gorg on 19.10.14.
 */
@Controller
@RequestMapping("/calculator")
public class CalculatorService {

    private final static Log log = LogFactory.getLog(CalculatorService.class);

    @Autowired
    LogRepository logRepository ;

    @Value("${history.page.size}")
    int pageSize ;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String log(@RequestParam String operation){
        log.debug(operation);
        LogEntry logEntry = new LogEntry(operation);
        logRepository.save(logEntry);

	    return null ;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Iterable<LogEntry> getLogEntries(@RequestParam(defaultValue = "0") int page){
        return logRepository.findAll(new PageRequest(page,pageSize,Sort.Direction.DESC,"creationTime"));
    }


}
