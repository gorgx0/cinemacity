package kalkulator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 *
 * Basic log entry class
 *
 * Created by gorg on 18.10.14.
 */
@Entity
public class LogEntry {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    private Date creationTime ;
    private String logEntry;

    public LogEntry(String logEntry) {
        this.logEntry = logEntry;
        this.creationTime = new Date();
    }

    public LogEntry() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getLogEntry() {
        return logEntry;
    }

    public void setLogEntry(String logEntry) {
        this.logEntry = logEntry;
    }

    @Override
    public String toString() {
        return "LogEntry{" +
                "id=" + id +
                ", creationTime=" + creationTime +
                ", logEntry='" + logEntry + '\'' +
                '}';
    }
}
