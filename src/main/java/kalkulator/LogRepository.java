package kalkulator;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by gorg on 18.10.14.
 */
public interface LogRepository extends PagingAndSortingRepository<LogEntry,Long> {


}
