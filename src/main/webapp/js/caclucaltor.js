// hide alert boxes
$("*[role=alert]").hide();

// initilize dispaly state variable
var resShown = false ;

// generic button handler
$("button").click(function(){
	console.log("Button clicked: "+this);
	if(resShown){
		$("#display").val("");
		resShown = false ;
	}
	var display = $("#display").val();
	display+=$(this).text();
	$("#display").val(display);
});

// prevent display from getting focus to disable typing into input directly
$("#display").focus(function(){
	$("#display").trigger("blur");
});

// unbind generic button click handler from special buttons
$("#btn_eq").unbind("click");
$("#alert_close_button").unbind("click");
$("#btn_history").unbind("click");
$("#btn_clear").unbind("click");
$("#btn_sqrt").unbind("click");
$("#history_close_button").unbind("click");
$("#history_close_button_2").unbind("click");
$("#history_prev").unbind("click");
$("#history_next").unbind("click");


// alert close button handler
$('.close').click(function() {
	$('.alert').hide();
});

// clear display handler
$("#btn_clear").click(function(){
    $("#display").val("");
    resShown = false ;
});

// sqrt button handler
$("#btn_sqrt").click(function(){
    var display = $("#display").val();
    if(display.length>0){
        display='sqrt('+display+')';
        $("#display").val(display);
    }else{
        console.log("sqrt pressed with empty display");
    }
});

// eq(=) button handler [do calculations]
$("#btn_eq").click(function(event){				
	console.log("do calculation");

    try{
        var display = $("#display").val();
        console.log("calculating expression: "+display);
        // logging operation
        $.post("/calculator",{operation: display})
            .done(function(){
                console.log("Operation logged");
            }).fail(function(err){
            	console.log(err);
                showError("Logging failed: "+err.statusText);
            })
        ;
        // parenthesis conversion
        display=display.replace('{','(');
        display=display.replace('}',')');
        // validation
        if(validate(display)){
            // replace sqrt
            display = display.replace('sqrt','Math.sqrt');
            var result = eval(display);
            if(isFinite(result)){
	            display = result ;
	            $("#display").val(display);
	            resShown = true ;
            }else{
            	console.log("calculation error");
		        showError("{calculation_error}");
            }
        }else{
            showError("{validation_failed}");
        }
    }catch(err){
        console.log("calculation error: "+err);
        showError(err);
    }
});

// non modal error message box
function showError(err){
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            message: err,
            title: '{calculation_error}'
        });
        $("#display").val("");

}

function validate(display) {
    return /^([\d()-]|sqrt)*([\d()+*-/]|sqrt)*/g.test(display) ;
}

// history button handler
$("#btn_history").click(function(){
    var page=0;

    $("#history_prev").click(function(){
        if(--page<0){
            page=0;
        }
        getPageOfHistory(page);
    });
    $("#history_next").click(function(){
        getPageOfHistory(++page);
    });

    getPageOfHistory(page);
});

// get page of history
function getPageOfHistory(page) {
    $.get("/calculator?page="+page,function(data){
        console.log("history data obtained for page="+page);
        if(data.content.length>0){
            $("#history_table_body").empty();
            data.content.forEach(function(entry){
                $("#history_table_body").append("<tr><td>" +new Date(entry['creationTime'])+
                    "</td><td>" +entry['logEntry']+
                    "</td></tr>");
            });
            $("#history_report").modal({keyboard: true});
            if(!data.firstPage){
                $("#history_prev").removeClass("hidden");
            }else{
                $("#history_prev").addClass("hidden");
            }
            if(!data.lastPage){
                $("#history_next").removeClass("hidden");
            }else{
                $("#history_next").addClass("hidden");
            }
        }else{
            showError("{hisotry_empty}");
        }
    }).
    fail(function(){
        showError("{getting_history_failed}");
    });
}
